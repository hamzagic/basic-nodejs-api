const Sequelize = require('sequelize');

const sequelize = require('../database/sequelizecon');

const OrderItem = sequelize.define('order_item', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    }
});

module.exports = OrderItem;