// const db = require('../database/connect'); //mysql2 only
const Sequelize = require('sequelize');

const sequelize = require('../database/sequelizecon');

const Product = sequelize.define('product', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false
    },
    price: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    imageUrl: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

module.exports = Product;

// The way it was done before using Sequelize
// module.exports = class Product {
//     constructor(title, description, price, imageUrl) {
//         this.title = title;
//         this.description = description;
//         this.price = price;
//         this.imageUrl = imageUrl;
//     }

//     save() {
//         return db.execute('INSERT INTO products (title, description, price, imageUrl) VALUES(?, ?, ?, ?)', [this.title, this.description, this.price, this.imageUrl]);
//     }

//     static fetchAll() {
//         return db.execute('SELECT * FROM products');
//     }

//     static getProductById(id) {
//         return db.execute('SELECT * FROM products WHERE products.id=?', [id]);
//     }
//  }
