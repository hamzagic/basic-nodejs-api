const Sequelize = require('sequelize');

const sequelize = require('../database/sequelizecon');

const CartItem = sequelize.define('cart_item', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    quantity: {
        type: Sequelize.INTEGER
    }
});

module.exports = CartItem;