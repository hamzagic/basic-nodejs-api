const Product = require('../models/product');

exports.getProducts = (req, res, next) => {
    Product.findAll().then(resp => {
        res.json({"data": resp});
    }).catch(err => console.log(err));
    // const products = Product.fetchAll().then(([rows, fieldData]) => {
    //     res.json({"data": rows});
    // }).catch(err => console.log(err));
};

exports.getProduct = (req, res, next) => {
    const productId = req.params.productId;

    Product.findByPk(productId)
    .then(resp => {
        res.json({"data": resp});
    })
    .catch(err => console.log(err));
    // const product = Product.getProductById(productId)
    // .then(([row]) => {
    //     res.json({"data": row});
    // })
    // .catch(err => console.log(err));
}

exports.addProduct = (req, res, next) => {
    const title = req.body.title;
    const description = req.body.description;
    const price = req.body.price;
    const imageUrl = req.body.imageUrl;
    console.log(req.user);
    Product.create({
       title: title,
       description: description,
       price: price,
       imageUrl: imageUrl,
       userId: req.user.id
    })
    .then(resp => {
        console.log(resp);
        res.status(201).json({"message": "product added"});

    })
    .catch(err => console.log(err));
    
    // const product = new Product(title, description, price, imageUrl);
    // product.save().then(() => {
    //     res.json({"message": "product added"});
    // }).catch(err => console.log(err));
}

exports.editProduct = (req, res, next) => {
    const prodId = req.body.productId;
    const title = req.body.title;
    const description = req.body.description;
    const price = req.body.price;
    const imageUrl = req.body.imageUrl;

    Product.findByPk(prodId)
    .then(product => {
        product.title = title;
        product.description = description;
        product.price = price;
        product.imageUrl = imageUrl;
        return product.save();
    })
    .then(result => {
        res.status(201).json({"data": result});
    })
    .catch(err => console.log(err));
}