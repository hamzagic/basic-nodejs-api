const express = require('express');
// const bodyParser = require('body-parser');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const errors = require('./controllers/errors');

const sequelize = require('./database/sequelizecon');
const Product = require('./models/product');
const User = require('./models/user');
const Cart = require('./models/cart');
const CartItem = require('./models/cart-item');
const Order = require('./models/order');
const OrderItem = require('./models/order-item');

const app = express();

// app.use(bodyParser.urlencoded({extended: false}));
app.use(express.urlencoded({extended: true}));

app.use((req, res, next) => {
    User.findByPk(1)
    .then(user => {
        req.user = user;
        next();
    })
    .catch(err => console.log(err));
});

app.use(express.json());

app.use(adminRoutes);

app.use(shopRoutes);

app.use(errors.notFound);

Product.belongsTo(User);
User.hasMany(Product);
User.hasOne(Cart);
Cart.belongsTo(User);
Cart.belongsToMany(Product, { through: CartItem });
Product.belongsToMany(Cart, { through: CartItem });
Order.belongsTo(User);
User.hasMany(Order);
Order.belongsToMany(Product, { through: OrderItem });

// sequelize.sync({force: true})
sequelize.sync()
.then(result => {
    return User.findByPk(1);
})
.then(user => {
    if(!user) {
        return User.create({ name: 'Kaspa', email: 'kaspa@test.com' });
    }
    return Promise.resolve(user);
})
.then(user => {
    return user.createCart();
})
.then(cart => {
    app.listen(5000);
})
.catch(err => console.log(err));

