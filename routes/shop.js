const express = require('express');

const cartController = require('../controllers/cart');

const router = express.Router();

router.get('/', (req, res, next) => {
    res.send("This is the root path!");
});

router.get('/cart', cartController.getCart);

module.exports = router;