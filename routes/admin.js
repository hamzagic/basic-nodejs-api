const express = require('express');

const productsController = require('../controllers/products');

const router = express.Router();

router.get('/product', productsController.getProducts);

router.get('/product/:productId', productsController.getProduct);

router.post('/product/add', productsController.addProduct);

router.post('/product/edit', productsController.editProduct);

module.exports = router;